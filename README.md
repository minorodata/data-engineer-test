#Data Engineer Test

This is the Minoro Python data engineering test. 

We're looking for solid reliable code and prioritising robust solutions that don't require second guessing over complex code. You'll be working with teams which need to pick up and maintain your code so simple and solid wins.

**Part 1**

In this repo is a data file mock_data.csv which contains poorly structured data and invalid characters. We're looking for a Python script which cleans up the file and makes it machine readable. Please comment your solution.

We're looking for solutions which work well with minimum fuss but if your solution is efficient and scales this is a bonus. 

**Part 2**

You've got a Redshift data warehouse experiencing some performance issues. How would you approach figuring out how to deal with this problem? There's no one right solution here, we're looking for a sensible approach to diagnosis.

Once you've completed these steps, please send your solutions to the email address provided.
